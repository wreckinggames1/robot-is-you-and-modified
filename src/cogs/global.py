from __future__ import annotations
import os
import aiohttp
import discord
import random
import re
import zipfile

from typing import BinaryIO, List, Optional, Tuple, Union
from datetime	import datetime
from discord.ext import commands
from functools   import partial
from inspect	 import Parameter
from io		  import BytesIO
from json		import load
from os		  import listdir
from PIL		 import Image, ImageChops, ImageFilter, ImageDraw
from string	  import ascii_lowercase
from src.utils   import *
from time		import time

# boosh
inactive_colors = {
	(0, 0): (0, 4),
	(1, 0): (0, 4),
	(2, 0): (1, 1),
	(3, 0): (0, 4),
	(4, 0): (0, 4),
	(5, 0): (6, 3),
	(6, 0): (6, 3),
	(0, 1): (1, 1),
	(1, 1): (1, 0),
	(2, 1): (2, 0),
	(3, 1): (3, 0),
	(4, 1): (4, 0),
	(5, 1): (5, 0),
	(6, 1): (6, 0),
	(0, 2): (0, 1),
	(1, 2): (1, 1),
	(2, 2): (2, 1),
	(3, 2): (1, 1),
	(4, 2): (4, 1),
	(5, 2): (5, 1),
	(6, 2): (6, 1),
	(0, 3): (0, 1),
	(1, 3): (1, 2),
	(2, 3): (2, 2),
	(3, 3): (4, 3),
	(4, 3): (1, 0),
	(5, 3): (5, 1),
	(6, 3): (0, 4),
	(0, 4): (6, 4),
	(1, 4): (1, 2),
	(2, 4): (6, 1),
	(3, 4): (6, 0),
	(4, 4): (3, 2),
	(5, 4): (5, 2),
	(6, 4): (6, 4),
}

# WIP
tips = {
	"0level":["Solution: || Go to the flag. ||"],
	"1level": ["Small tip: || You can *break* sentences. || ", "Solution: || Break 'wall is stop' sentence, then make: 'flag is win' ||"],
	"2level": ["Small tip: || 'defeat' property only kills objects, that have 'you' property. ||", "Normal tip: || Only baba has 'you' property. ||", "Solution: || Push either rocks or text in a line, so it breaks 'skull is defeat' text, and go for a flag. ||"],
	"3level": ["Small tip: ||  Not only flag can be a win.  ||", "Normal tip: || You need to sacrifice one rock to win, but you should keep the other. ||", "Solution: || Break one water tile with one rock, change two sentences at the bottom, so you have only 'rock is win', and just step on the rock. ||"],
	"4level": ["Small tip: ||  You can push something in the flag. ||", "Normal tip: || Not only baba can be 'you'. ||", "Solution: || Stack some pillars in a line, push that line into flag, then make 'pillar is you'. ||"],
	"5level": ["Normal tip: || You have only two rules to change, try every combination. ||", "Solution: || Break 'wall is stop' sentence, get the 'wall' word, put it instead of 'baba', so it would be 'wall is you' and then just go for a flag. ||"],
	"6level": ["Small tip: || Wall is not stop. ||", "Normal tip: || You need to make two sentences, with only one 'is'. ||", "Big tip: || You need to make a cross, with two sentences. ||", "Solution: || Make it like this: ||", "||		flag	   ||", "|| baba is you ||", "||		 win	   ||"],
	"7level": ["Small tip: || You can make a sentence without using properties. ||", "Normal tip: || Objects can be made into other objects ||", "Solution: || Firstly, stop robot from breaking 'baba is you', by putting it all the way down, then wait for robot to go after skulls, then you should make 'robot is baba' and get that win out, use the 'is' from 'robot is baba' to make 'baba is win'.  ||"],
	"8level": ["Small tip: || You can transform objects into other objects. ||", "Big tip: || Try to transform wall to jelly. ||", "Solution: || Make 'wall is jelly' and break 'jelly is stop', so now you can escape, then just do 'jelly is win'. ||"],
	"9level": ["Small tip: || Mess around with the empty word a bit. ||", "Big tip: || Empty can make other things empty. ||", "Solution: || Either make 'belt is empty' to remove the belt, clearing the path to the flag, or make 'empty is flag' for all empty space to become flags ||"],
	"10level": ["Small tip: || You can change directions of objects, by pushing them. ||", "Big tip: || Things that have 'move' property can push things, that have 'push' property. ||", "Solution: || Break 'keke is you' sentence, form 'keke is push' sentence, push keke so he is facing the love, form 'love is push' sentence, then 'keke is move', so keke pushes out the love. Then walk to the love to win ||"],
	"11level": ["Small tip: || Anni has nothing to do with the solution, it's just there to distract you. ||", "Normal tip: || You can make the empty space any object, or make any object empty space, if you use 'empty' word correctly ||", "Solution: || form 'door is empty' sentence to remove the doors, then form 'empty is baba' sentence to make all empty space baba. Some babas appear in the box the flag is in, guide those babas to the flag to win. ||"],
	"12level": ["This is a level from hempuli's hidden levels, you can try recreate this, and solve, but there will be no solution here (at least for now). Only story in-game levels has solutions."],
	"13level": ["Small tip: || Try to stop the spread of babas at the beginning, they are just there to annoy you ||", "Normal tip: || Everything that has 'defeat' property, destroys everything that has 'you' property. ||", "Solution: || Break 'empty is baba' sentence, form 'rock is you' sentence so the Rock dissapears, form 'empty is baba' sentence again for a short time, so at least one baba is going to be outside of the box. Then bring the 'win' text to 'ice is' to form 'ice is win', and walk to the ice to win. ||"],
	"14level": ["This is a level from hempuli's hidden levels, you can try recreate this, and solve, but there will be no solution here (at least for now). Only story in-game levels has solutions."],
	"15level": ["Small tip: || Objects can have two properties at the same time. ||", "Big tip: || 'open' text opens ANYTHING that is 'shut'. ||", "Solution: || Form a cross sentence, so both the sentences 'key is push' and 'key is open' use the same 'is' text, use the key to open the door, then form sentence 'door is shut' vertically, so 'door is shut' sentence is unchanged. door will immediately be opened, revealing the path to the flag. ||"],
	"16level": [],
	"17level": ["Small tip: || There is nothing special in the 'box', it's just a placeholder object. ||", "Normal tip: || You don't need two object, one keke is enough to complete the level. ||", "Solution: || Make 'box is keke', sacrifice one keke in the upper water tile, get 'box has' from upper room,  and form 'box is keke is you' and 'keke has box' vertically, then go to the flag. ||"],
	"18level": ["Small tip: || If X has X, and it gets opened, X is still existant ||", "Normal tip: || If something is shut and open at the same time, it basically dissapears, unless it has something ||", "Solution: || Break 'rock is open' sentence, then, by using both babas, and an 'is' text, shift the 'baba is you' sentence upwards a bit. Then put the 'rock is' text right above the 'baba is' text. Then, proceed to move the other rock text, which is not above the 'baba' text, but under the 'you' text. After that, push the 'rock' text under the 'you' text upwards, so 'baba is rock' and 'rock is you' sentences are formed. Move the 'baba is rock' sentence away, to make place for the 'rock has rock' sentence. Then, form the 'rock has rock' sentence, vertically, so 'rock is you' is unaffected. After all that stuff form the sentence 'all is open', wich is going to remove the walls, opening access to the 'weak' text. Immediately break the sentence 'all is open' again, and, in the upper right corner, form 'baba on rock is win' sentence. And then, break the sentence 'rock has rock', and form the sentence 'rock has baba'. Finally, form the sentence all is weak, and try to destroy only one rock, so a baba will appear. Break the 'all is weak' sentence, and walk on top of baba to win. ||"],
	"19level": ["Small tip: || The 'flag not on wall is win' text is just there to distract you, ignore it ||", "Big tip: || Try to form '[some object] is all' and see what happens! ||", "Solution: || Break 'rock is move' sentence, and form 'rock is push' sentence. Then push the rock to a place that has more space. After that, form the sentence 'love is move' and 'rock is all' VERTICALLY (I dont recommend moving around while that sentence is active). After waiting 2 - 5 turns, break the sentence 'rock is all' by pushing the 'rock' text into the box where 'wall is stop' and 'door is shut' is stored, and break the 'love is move' sentence. Then retrieve the 'is' text from 'is all' by pushing the 'all' into the little hole on the right and moving the 'is' text up 5 tiles. Use this text to form 'love is push' sentence vertically, and 'love is open' horizontally. Push the love to open the doors to get to the flag. ||"],
	"20level": ["Small tip: || Two things can be you at the same time. ||", "Big tip: || You can remove the 'sink' and 'and' text from 'baba is you and sink' , to then retrieve the 'wall' text ||", "Solution: || Break the 'baba is you and sink' sentence by removing 'sink' and 'and'. Bring the 'and' text behind the 'baba' text, then bring the 'wall' text behind the 'and' text, so 'wall and baba is you' sentence is made. Then walk over to the jelly to sink it, revealing the path to the flag. ||"],
	"21level": ["Small tip: || Experiment with the 'all' text a bit! ||", "Big tip: || Try to use the 'sink' text to your advantage ||", "Solution: || Form the sentence 'all is you' using the 'is' text from 'water is sink'. after that break the sentence 'skull is you'. Use the 'is' text to form 'all is sink', then try to sink the wall to free the 'win' text. Then replace the 'sink' in 'all is sink' with 'win', to win ||"],
	"22level": [],
	"23level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"24level": ["This is a level from hempuli's hidden levels, you can try recreate this, and solve, but there will be no solution here (at least for now). Only story in-game levels has solutions."],
	"25level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"26level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"27level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"28level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"29level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"30level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"31level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"32level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"33level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"34level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"35level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"36level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"37level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"38level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"39level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"40level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"41level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"42level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"43level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"44level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"45level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"46level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"47level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"48level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"49level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"50level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"51level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"52level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"53level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"54level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"55level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"56level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"57level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"58level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"59level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"60level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"61level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"62level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"63level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"64level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"65level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"66level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"67level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"68level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"69level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"70level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"71level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"72level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"73level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"74level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"75level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"76level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"77level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"78level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"79level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"80level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"81level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"82level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"83level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"84level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"85level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"86level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"87level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"88level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"89level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"90level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"91level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"92level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"93level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"94level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"95level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"96level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"97level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"98level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"99level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"100level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"101level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"102level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"103level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"104level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"105level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"106level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"107level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"108level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"109level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"110level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"111level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"112level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"113level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"114level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"115level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"116level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"117level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"118level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"119level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"120level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"121level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"122level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"123level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"124level": ["Small tip: || 'keke' text is kinda useless, dont try to use it in a sentence ||", "Normal tip: || You have to push two objects in there, so you must somehow stack them ||", "Solution: || Form the sentence 'rock on text is push', then proceed to push the 'keke' text on top of the rock. Then go to the right area, sink the rock in the water to create a path to the 'flag' text. And finally, use the 'keke' text to push the 'flag' text up, to create the sentence 'flag is win ||"],
	"125level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"126level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"127level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"128level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"129level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"130level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"131level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"132level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"133level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"134level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"135level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"136level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"137level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"138level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"139level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"140level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"141level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"142level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"143level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"144level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"145level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"146level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"147level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"148level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"149level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"150level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"151level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"152level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"153level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"154level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"155level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"156level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"157level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"158level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"159level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"160level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"161level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"162level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"163level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"164level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"165level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"166level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"167level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"168level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"169level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"170level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"171level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"172level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"173level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"174level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"175level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"176level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"177level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"178level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"179level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"180level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"181level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"182level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"183level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"184level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"185level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"186level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"187level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"188level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"189level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"190level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"191level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"192level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"193level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"194level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"195level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"196level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"197level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"198level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"199level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"200level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"201level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"202level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"203level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"204level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"205level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"206level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"207level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"208level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"209level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"210level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"211level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"212level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"213level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"214level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"215level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"216level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"217level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"218level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"219level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"220level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"221level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"222level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"223level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"224level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"225level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"226level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"227level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"228level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"229level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"230level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"231level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"232level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"233level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"234level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"235level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"236level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"237level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"238level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"239level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"240level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"241level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"242level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"243level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"244level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"245level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"246level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"247level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"248level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"249level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"250level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"251level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"252level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"253level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"254level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"255level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"256level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"257level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"258level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"259level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"260level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"261level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"262level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"263level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"264level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"265level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"266level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"267level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"268level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"269level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"270level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"271level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"272level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"273level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"274level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"275level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"276level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"277level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"278level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"279level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"280level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"281level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"282level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"283level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"284level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"285level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"286level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"287level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"288level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"289level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"290level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"291level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"292level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"293level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"294level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"295level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"296level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"297level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"298level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"299level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"300level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"301level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"302level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"303level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"304level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"305level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"306level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"307level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"308level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"309level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"310level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"311level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"312level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"313level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"314level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"315level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"316level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"317level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"318level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"319level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"320level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"321level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"322level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"323level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"324level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"325level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"326level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"327level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"328level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"],
	"329level": ["Small tip: || test ||", "Normal tip: || test ||", "Solution: || some solution ||"]
}

def generate_tips(level_id: int) -> str:
	final = ""
	tip_str = tips[level_id]
	for n in tip_str:
		final += "\n" + n
	return final	

valid_colors = {
	"maroon":(2, 1), # Not actually a word in the game
	"red":(2, 2),
	"orange":(2, 3),
	"yellow":(2, 4),
	"lime":(5, 3),
	"green":(5, 2),
	"cyan":(1, 4),
	"blue":(1, 3),
	"purple":(3, 1),
	"pink":(4, 1),
	"rosy":(4, 2),
	"grey":(0, 1),
	"gray":(0, 1),
	"black":(0, 4),
	"silver":(0, 2),
	"white":(0, 3),
	"brown":(6, 1),
}

valid_true_colors = {
	"blurple": "#7289DA",
	"dark_blurple": "#4E5D94",
	"dark": "#23272A",
}

def flatten(items, seqtypes=(list, tuple)):
	'''Flattens nested iterables, of speficied types.
	Via https://stackoverflow.com/a/10824086
	'''
	for i, _ in enumerate(items):
		while i < len(items) and isinstance(items[i], seqtypes):
			items[i:i+1] = items[i]
	return items

def try_index(string: str, value: str) -> int:
	'''Returns the index of a substring within a string.
	Returns -1 if not found.
	'''
	index = -1
	try:
		index = string.index(value)
	except:
		pass
	return index

class SplittingException(BaseException):
	pass

# Splits the "text_x,y,z..." shortcuts into "text_x", "text_y", ...
def split_commas(grid: List[List[str]], prefix: str):
	for row in grid:
		to_add = []
		for i, word in enumerate(row):
			if "," in word:
				if word.startswith(prefix):
					each = word.split(",")
					expanded = [each[0]]
					expanded.extend([prefix + segment for segment in each[1:]])
					to_add.append((i, expanded))
				else:
					raise SplittingException(word)
		for change in reversed(to_add):
			row[change[0]:change[0] + 1] = change[1]
	return grid

def find_nth(string, substr, n):
	start = string.find(substr)
	while start >= 0 and n > 1:
		start = string.find(substr, start+len(substr))
		n -= 1
	return start


class GlobalCog(commands.Cog, name="Baba Is You"):
	def __init__(self, bot: commands.Bot):
		self.bot = bot
		with open("config/leveltileoverride.json") as f:
			j = load(f)
			self.level_tile_override = j

		with open("config/banned_words.json") as f:
			# Load the list of words, then make all variations of them.
			raw_list = load(f)
			# Final list definition
			refined_list = []
			for word in raw_list:
				# The word itself, ex.: "banned"
				refined_list.append(word)
				# Word with "~" at the start of each character + characters are splitted, ex.: "~b ~a ~n ~n ~e ~d"
				refined_list.append(" ".join([f"~{x}" for x in word]))
				# Check if there are more spaces than one in the banned word
				
				# WARNING: Very mess code, I would even say garbage. TODO: CLEAN THIS UP AAAAAAAH

				weird_word = " ".join([f"~{x}" for x in word])
				if weird_word.count(" ") > 1:
					if weird_word.count(" ") % 2 != 0:
						refined_list.append(weird_word[:(find_nth(weird_word, " ", (weird_word.count(" ") / 2)))] + " /" + weird_word[(find_nth(weird_word, " ", (weird_word.count(" ") / 2))) + 1:])
						refined_list.append(weird_word[:(find_nth(weird_word, " ", (weird_word.count(" ") / 2)))] + "/" + weird_word[(find_nth(weird_word, " ", (weird_word.count(" ") / 2))) + 1:])
						refined_list.append(weird_word[:(find_nth(weird_word, " ", (weird_word.count(" ") / 2)))] + "/ " + weird_word[(find_nth(weird_word, " ", (weird_word.count(" ") / 2))) + 1:])
					else:
						refined_list.append(weird_word[:(find_nth(weird_word, " ", int((weird_word.count(" ") / 2))))] + " /" + weird_word[(find_nth(weird_word, " ", int((weird_word.count(" ") / 2)))) + 1:])
						refined_list.append(weird_word[:(find_nth(weird_word, " ", int((weird_word.count(" ") / 2))))] + "/" + weird_word[(find_nth(weird_word, " ", int((weird_word.count(" ") / 2)))) + 1:])
						refined_list.append(weird_word[:(find_nth(weird_word, " ", int((weird_word.count(" ") / 2))))] + "/ " + weird_word[(find_nth(weird_word, " ", int((weird_word.count(" ") / 2)))) + 1:])

						refined_list.append(weird_word[:(find_nth(weird_word, " ", int((weird_word.count(" ") / 2))))] + " /" + weird_word[(find_nth(weird_word, " ", int((weird_word.count(" ") / 2)) + 1)) + 1:])
						refined_list.append(weird_word[:(find_nth(weird_word, " ", int((weird_word.count(" ") / 2))))] + "/" + weird_word[(find_nth(weird_word, " ", int((weird_word.count(" ") / 2)) + 1)) + 1:])
						refined_list.append(weird_word[:(find_nth(weird_word, " ", int((weird_word.count(" ") / 2))))] + "/ " + weird_word[(find_nth(weird_word, " ", int((weird_word.count(" ") / 2)) + 1)) + 1:])
				else:
					# Words with "~" at the start of each character + characters are splitted, AND with empty space being replaced with "/", " /" and "/ ", ex.: "~b ~a ~n/ ~n ~e ~d", "~b ~a ~n / ~n ~e ~d", "~b ~a ~n /~n ~e ~d"
					refined_list.append((" ".join([f"~{x}" for x in word])).replace(" ", " /"))
					refined_list.append((" ".join([f"~{x}" for x in word])).replace(" ", "/"))
					refined_list.append((" ".join([f"~{x}" for x in word])).replace(" ", "/ "))
				# Word with "/~~&~~/" in the middle, ex.: "ban/~~&~~/ned"
				refined_list.append(word[:int(len(word) / 2)] + "/~~&~~/" + word[int(len(word) / 2):])
				# Word with " " in the middle, ex.: "ban ned"
				refined_list.append(word[:int(len(word) / 2)] + " " + word[int(len(word) / 2):])
				# Word with "/" in the middle, ex.: "ban/ned"
				refined_list.append(word[:int(len(word) / 2)] + "/" + word[int(len(word) / 2):])
				# Some previous variations with uneven words count.
				if len(word) != int(len(word)):
					refined_list.append(word[:int(len(word) / 2) + 1] + "/~~&~~/" + word[int(len(word) / 2) + 1:])
					refined_list.append(word[:int(len(word) / 2) + 1] + " " + word[int(len(word) / 2) + 1:])
					refined_list.append(word[:int(len(word) / 2) + 1] + "/" + word[int(len(word) / 2) + 1:])
			self.banned_words = refined_list

	# Check if the bot is loading
	async def cog_check(self, ctx: commands.Context):
		'''Only if the bot is not loading assets'''
		return not self.bot.loading

	def save_frames(self, frames: List[Image.Image], fp: Union[str, BinaryIO]):
		'''Saves a list of images as a gif to the specified file path.'''
		frames[0].save(fp, "GIF",
			save_all=True,
			append_images=frames[1:],
			loop=0,
			duration=200,
			disposal=2, # Frames don't overlap
			transparency=255,
			background=255,
			optimize=False # Important in order to keep the color palettes from being unpredictable
		)
		if not isinstance(fp, str): fp.seek(0)

	def make_meta(self, name: str, img: Image.Image, meta_level: int) -> Image.Image:
		if meta_level > 3:
			raise ValueError(name, meta_level, "meta")
		elif meta_level == 0:
			return img

		original = img.copy()
		final = img
		for i in range(meta_level):
			_, _, _, alpha = final.convert("RGBA").split()
			base = Image.new("L", (final.width + 6, final.width + 6))
			base.paste(final, (3, 3), alpha)
			base = ImageChops.invert(base)
			base = base.filter(ImageFilter.FIND_EDGES)
			ImageDraw.floodfill(base, (0, 0), 0)
			base = base.crop((2, 2, base.width - 2, base.height - 2))
			base = base.convert("1")
			final = base.convert("RGBA")
			final.putalpha(base)
		if meta_level >= 2 and meta_level % 2 == 0:
			final.paste(original, (i + 1, i + 1), original.convert("L"))
		elif meta_level >= 2 and meta_level % 2 == 1:
			final.paste(ImageChops.invert(original), (i + 1, i + 1), original.convert("L"))
		
		return final
		

	def render(
		self,
		word_grid: List[List[List[Tile]]],
		width: int,
		height: int,
		*,
		palette: str = "default",
		images: Optional[List[Image.Image]] = None,
		image_source: str = "vanilla",
		out: str = "target/render/render.gif",
		background: Optional[Tuple[int, int]] = None,
		rand: bool = False,
		use_overridden_colors: bool = False,
		raw: bool = False
	):
		'''Takes a list of Tile objects and generates a gif with the associated sprites.

		out is a file path or buffer. render will be saved there, otherwise to `target/render/render.gif`.

		palette is the name of the color palette to refer to when rendering.

		images is a list of background image filenames. Each image is retrieved from `data/images/{imageSource}/image`.

		background is a palette index. If given, the image background color is set to that color, otherwise transparent. Background images overwrite this. 
		'''
		frames = []
		palette_img = Image.open(f"data/palettes/{palette}.png").convert("RGB")

		# Calculates padding based on image sizes
		left_pad = 0
		right_pad = 0
		up_pad = 0
		down_pad = 0

		# Get sprites to be pasted
		cache= {}
		imgs = []
		for frame in range(3):
			temp_frame = []
			for y, row in enumerate(word_grid):
				temp_row = []
				for x, stack in enumerate(row):
					temp_stack = []
					for _z, tile in enumerate(stack):
						if tile.name is None:
							continue
						# Custom tiles
						if tile.custom:
							# Custom tiles are already rendered, and have their meta level applied properly
							img = tile.images[frame]
							if y == 0:
								diff = img.size[1] - 24
								if diff > up_pad:
									up_pad = diff
							if y == len(word_grid) - 1:
								diff = img.size[1] - 24
								if diff > down_pad:
									down_pad = diff
							if x == 0:
								diff = img.size[0] - 24
								if diff > left_pad:
									left_pad = diff
							if x == len(row) - 1:
								diff = img.size[0] - 24
								if diff > right_pad:
									right_pad = diff
							temp_stack.append(img)
							continue
						if rand:
							# Random animations
							animation_offset = (hash(x + y) + frame) % 3
						else:
							animation_offset = frame
						# Certain sprites have to be hard-coded, since their file paths are not very neat
						if tile.name in ("icon"):
							path = f"data/sprites/vanilla/{tile.name}.png"
						elif tile.name in ["smiley", "hi"] or tile.name.startswith("icon"):
							path = f"data/sprites/vanilla/{tile.name}_1.png"
						elif tile.name == "default":
							path = f"data/sprites/vanilla/default_{animation_offset + 1}.png"
						else:
							maybe_sprite = self.bot.get_cog("Admin").tile_data.get(tile.name).get("sprite")
							if maybe_sprite != tile.name:
								path = f"data/sprites/{tile.source}/{maybe_sprite}_{tile.variant}_{animation_offset + 1}.png"
							else:
								path = f"data/sprites/{tile.source}/{tile.name}_{tile.variant}_{animation_offset + 1}.png"
						if tile.color is None:
							base = None
							if use_overridden_colors:
								base = self.level_tile_override.get(tile.name)
							base = base if base is not None else self.bot.get_cog("Admin").tile_data[tile.name]
							tile.color = base.get("active")
							if tile.color is None:
								tile.color = base.get("color")
							tile.color = tuple(map(int, tile.color))
						img = cached_open(path, cache=cache, is_image=True).convert("RGBA")
						img = self.make_meta(tile.name, img, tile.meta_level)
						if tile.true_color == None:
							c_r, c_g, c_b = palette_img.getpixel(tile.color)
						else:
							c_r, c_g, c_b = tile.true_color
						if raw:
							color_matrix = (1, 0, 0, 0,
											0, 1, 0, 0,
										0, 0, 1, 0,)
							_r, _g, _b, alpha = img.split()
							img = img.convert("RGB").convert("RGB", matrix=color_matrix)
							img.putalpha(alpha)
						else:
							_r, _g, _b, a = img.split()
							color_matrix = (c_r / 256, 0, 0, 0,
										0, c_g / 256, 0, 0,
									0, 0, c_b / 256, 0)
							img = img.convert("RGB").convert("RGB", matrix=color_matrix)
							img.putalpha(a)

						temp_stack.append(img)
						# Check image sizes and calculate padding
						if y == 0:
							diff = img.size[1] - 24
							if diff > up_pad:
								up_pad = diff
						if y == len(word_grid) - 1:
							diff = img.size[1] - 24
							if diff > down_pad:
								down_pad = diff
						if x == 0:
							diff = img.size[0] - 24
							if diff > left_pad:
								left_pad = diff
						if x == len(row) - 1:
							diff = img.size[0] - 24
							if diff > right_pad:
								right_pad = diff
					temp_row.append(temp_stack)
				temp_frame.append(temp_row)
			imgs.append(temp_frame)

		 
		for i,frame in enumerate(imgs):
			# Get new image dimensions, with appropriate padding
			total_width = len(frame[0]) * 24 + left_pad + right_pad 
			total_height = len(frame) * 24 + up_pad + down_pad 

			# Montage image
			# bg images
			if bool(images) and image_source is not None:
				render_frame = Image.new("RGBA", (total_width, total_height))
				# for loop in case multiple background images are used (i.e. baba's world map)
				for image in images:
					overlap = Image.open(f"data/images/{image_source}/{image}_{i + 1}.png") # i + 1 because 1-indexed
					mask = overlap.getchannel("A")
					render_frame.paste(overlap, mask=mask)
			# bg color
			elif background is not None:
				palette_img = Image.open(f"data/palettes/{palette}.png").convert("RGB") # ensure alpha channel exists, even if blank
				palette_color = palette_img.getpixel(background)
				render_frame = Image.new("RGBA", (total_width, total_height), color=palette_color)
			# neither
			else: 
				render_frame = Image.new("RGBA", (total_width, total_height))

			# Pastes each image onto the frame
			# For each row
			y_offset = up_pad # For padding: the cursor for example doesn't render fully when alone
			for row in frame:
				# For each image
				x_offset = left_pad # Padding
				for stack in row:
					for tile in stack:
						if tile is not None:
							if isinstance(tile, list):
								elem = tile[i]
							else:
								elem = tile

							width = elem.width
							height = elem.height
							# For tiles that don't adhere to the 24x24 sprite size
							offset = (x_offset + (24 - width) // 2, y_offset + (24 - height) // 2)

							render_frame.paste(elem, offset, elem)
					x_offset += 24
				y_offset += 24

			if not raw:
				# Resizes to 200%
				render_frame = render_frame.resize((2 * total_width, 2 * total_height), resample=Image.NEAREST)
			# Saves the final image
			frames.append(render_frame)

		self.save_frames(frames, out)

	def handle_variants(self, grid: List[List[List[str]]], *, tile_borders: bool = False, is_level: bool = False, palette: str = "default") -> List[List[List[Tile]]]:
		'''Appends variants to tiles in a grid.

		Returns a grid of `Tile` objects.

		* No variant -> "0" sprite variant, default color
		* Shortcut sprite variant -> The associated sprite variant
		* Given sprite variant -> Same sprite variant
		* Sprite variants for a tiling object -> Sprite variant auto-generated according to adjacent tiles
		* Shortcut color variant -> Associated color index 
		* (Source based on tile)
		If tile_borders is given, sprite variants depend on whether the tile is adjacent to the edge of the image.
		'''

		width = len(grid[0])
		height = len(grid)
		palette_img = Image.open(f"data/palettes/{palette}.png").convert("RGB")

		clone_grid = [[[word for word in stack] for stack in row] for row in grid]
		for y, row in enumerate(clone_grid):
			for x, stack in enumerate(row):
				for z, word in enumerate(stack):
					# True color flag is setting to False every start of the loop, so not every tile in the grid would be custom
					true_color_flag = False
					tile = word
					final = Tile()
					if tile in ("-", "empty"):
						grid[y][x][z] = final
					else:
						# Get variants from string
						if ":" in tile:
							segments = tile.split(":")
							tile = segments[0]
							final.name = tile
							variants = segments[1:]
						else:
							variants = []
							final.name = word
						# Easter egg
						if "hide" in variants:
							grid[y][x][z] = Tile()
							continue 
						# Certain tiles from levels are overridden with other tiles
						tile_data = self.bot.get_cog("Admin").tile_data.get(tile)
						if is_level:
							if self.level_tile_override.get(tile) is not None:
								tile_data = self.level_tile_override[tile]
						# Apply globally valid variants
						delete = []
						for i, variant in enumerate(variants):
							# COLORS
							if valid_colors.get(variant) is not None:
								final.color = valid_colors.get(variant)
								delete.append(i)
							# True colors ( aka "hex colouring" )
							elif valid_true_colors.get(variant) is not None:
								true_color = valid_true_colors.get(variant).lstrip("#")
								final.true_color = list(int(true_color[i:i+2], 16) for i in (0, 2, 4))
								true_color_flag = True
								delete.append(i)
							elif "#" in variant:
								true_color = variant.lstrip("#")
								final.true_color = list(int(true_color[i:i+2], 16) for i in (0, 2, 4))
								true_color_flag = True
								delete.append(i)
							# META SPRITES
							elif variant in ("meta", "m"):
								final.meta_level += 1
								delete.append(i)
							else:
								match = re.fullmatch("m(\d)", variant)
								if match:
									level = int(match.group(1))
									if level > 3:
										raise FileNotFoundError
									final.meta_level = level
									delete.append(i)
						# Need to do, cause I can't compare undefined vars
						try:
							final.true_color = final.true_color
						except:
							final.true_color = None
						try:
							true_color_flag = true_color_flag
						except:
							true_color_flag = False
						# This needs to be evaluated after color variants, so it can compute the proper inactive color
						if variants.count("inactive") > 0:
							# If an active variant exists, the default color is inactive
							for i in range(variants.count("inactive")):
								if true_color_flag:
									new_color = []
									for color in final.true_color:
										color = color - 40
										new_color.append(color)
									final.true_color = new_color
									variants.remove("inactive")
								elif tile_data and tile_data.get("active"):
									if final.color is None or tile_data["color"] == final.color:
										final.color = tuple(map(int, tile_data["color"]))
									else:
										final.color = inactive_colors[final.color or (0, 3)]
									variants.remove("inactive")
								else:
									final.color = inactive_colors[final.color or (0, 3)]
									variants.remove("inactive")
						delete.reverse()
						for i in delete:
							if len(variants) > 0:
								variants.pop(i)
							else:
								pass
						# Force custom-rendered text
						# This has to be rendered beforehand (each sprite can't be generated separately)
						# because generated sprites uses randomly picked letters
						if tile_data is None or any(x in variants for x in ("property","noun", "letter")) or (true_color_flag and tile.startswith("text_")):
							if tile.startswith("text_"):
								final.custom = True
								if "property" in variants:
									if "right" in variants or "r" in variants:
										final.style = "propertyright"
									elif "up" in variants or "u" in variants:
										final.style = "propertyup"
									elif "left" in variants or "l" in variants:
										final.style = "propertyleft"
									elif "down" in variants or "d" in variants:
										final.style = "propertydown"
									else:
										final.style = "property"
								if "noun" in variants:
									final.style = "noun"
								if "letter" in variants:
									final.style = "letter"
								if final.color is None and final.true_color is None:
									# Seed used for RNG to ensure that two identical sprites get generated the same way regardless of stack position
									final.images = self.generate_tile(tile[5:], (1,1,1), final.style, final.meta_level, y*100+x) 
								elif final.color or final.true_color:
									whites = self.generate_tile(tile[5:], (1,1,1), final.style, final.meta_level, y*100+x)
									colored = []
									for im in whites:
										if true_color_flag == True:
											c_r, c_g, c_b = final.true_color
										else:
											c_r, c_g, c_b = palette_img.getpixel(final.color)								  
										_r, _g, _b, a = im.split()
										color_matrix = (c_r / 256, 0, 0, 0,
														0, c_g / 256, 0, 0,
													0, 0, c_b / 256, 0)
										color = im.convert("RGB").convert("RGB", matrix=color_matrix)
										color.putalpha(a)
										colored.append(color)
									final.images = colored
								grid[y][x][z] = final
								continue
							raise FileNotFoundError(tile)

						# Tiles from here on are guaranteed to exist
						final.source = tile_data.get("source") or "vanilla"
						tiling = tile_data.get("tiling")
						direction = 0
						animation_frame = 0
						
						# Is this a tiling object (e.g. wall, water)?
						if tiling == "1":
							#  The final variation of the tile
							out = 0

							# Tiles that join together
							def does_tile(stack):
								return any(t == tile or t == "level" for t in stack)

							# Is there the same tile adjacent right?
							if x != width - 1:
								# The tiles right of this (with variants stripped)
								adjacent_right = [t.split(":")[0] for t in clone_grid[y][x + 1]]
								if does_tile(adjacent_right):
									out += 1
							if tile_borders:
								if x == width - 1:
									out += 1

							# Is there the same tile adjacent above?
							if y != 0:
								adjacent_up = [t.split(":")[0] for t in clone_grid[y - 1][x]]
								if does_tile(adjacent_up):
									out += 2
							if tile_borders:
								if y == 0:
									out += 2

							# Is there the same tile adjacent left?
							if x != 0:
								adjacent_left = [t.split(":")[0] for t in clone_grid[y][x - 1]]
								if does_tile(adjacent_left):
									out += 4
							if tile_borders:
								if x == 0:
									out += 4

							# Is there the same tile adjacent below?
							if y != height - 1:
								adjacent_down = [t.split(":")[0] for t in clone_grid[y + 1][x]]
								if does_tile(adjacent_down):
									out += 8
							if tile_borders:
								if y == height - 1:
									out += 8
							
							# Stringify
							final.variant = str(out)
							if is_level: 
								grid[y][x][z] = final
								continue

						# Apply actual sprite variants
						for variant in variants:
							# SPRITE VARIANTS
							# TODO: clean this up big time
							if tiling == "-1":
								if variant in ("r", "right"):
									direction = 0
								elif variant == "0":
									final.variant = variant
								elif is_level:
									direction = 0
								else:
									raise FileNotFoundError(word)
							elif tiling == "0":
								if variant in ("r", "right"):
									direction = 0
								elif variant in ("u", "up"):
									direction = 1
								elif variant in ("l", "left"):
									direction = 2
								elif variant in ("d", "down"):
									direction = 3
								elif variant in ( "0", "8", "16", "24"):
									final.variant = variant
								elif is_level:
									direction = 0
								else:
									raise FileNotFoundError(word)
							elif tiling == "1":
								if variant in (
									"0", "1", "2", "3",
									"4", "5", "6", "7",
									"8", "9", "10", "11",
									"12", "13", "14", "15",
								):
									final.variant = variant
								elif is_level:
									raise FileNotFoundError(word)
							elif tiling == "2":
								if variant in ("r", "right"):
									direction = 0
								elif variant in ("u", "up"):
									direction = 1
								elif variant in ("l", "left"):
									direction = 2
								elif variant in ("d", "down"):
									direction = 3
								elif variant == "a0": 
									animation_frame = 0
								elif variant == "a1": 
									animation_frame = 1
								elif variant == "a2": 
									animation_frame = 2
								elif variant == "a3": 
									animation_frame = 3
								elif variant in ("s", "sleep"): 
									animation_frame = -1
								elif variant in (
									"31", "0", "1", "2", "3", 
									"7", "8", "9", "10", "11",
									"15", "16", "17", "18", "19",
									"23", "24", "25", "26", "27",
								): 
									final.variant = variant
								elif is_level:
									direction = 0
								else:
									raise FileNotFoundError(word)
							elif tiling == "3":
								if variant in ("r", "right"):
									direction = 0
								elif variant in ("u", "up"):
									direction = 1
								elif variant in ("l", "left"):
									direction = 2
								elif variant in ("d", "down"):
									direction = 3
								elif variant == "a0": 
									animation_frame = 0
								elif variant == "a1": 
									animation_frame = 1
								elif variant == "a2": 
									animation_frame = 2
								elif variant == "a3": 
									animation_frame = 3
								elif variant in (
									"0", "1", "2", "3", 
									"8", "9", "10", "11",
									"16", "17", "18", "19",
									"24", "25", "26", "27",
								): 
									final.variant = variant
								elif is_level:
									direction = 0
								else:
									raise FileNotFoundError(word)
							elif tiling == "4":
								if variant in ("r", "right"):
									direction = 0
								else:
									if is_level:
										direction = 0
									elif variant == "a1": 
										animation_frame = 1
									elif variant == "a2": 
										animation_frame = 2
									elif variant == "a3": 
										animation_frame = 3
									elif is_level:
										direction = 0
									elif variant in (
										"0", "1", "2", "3", 
									):
										final.variant = variant
									elif is_level:
										direction = 0
									else:
										raise FileNotFoundError(word)

						# Compute the final variant, if not already set
						final.variant = final.variant or (8 * direction + animation_frame) % 32

						# Finally, push the sprite to the grid
						grid[y][x][z] = final
		return grid

	@commands.group(invoke_without_command=True)
	@commands.cooldown(5, 8, commands.BucketType.channel)
	async def make(self, ctx: commands.Context, text: str, color: str = None, style: str = "noun", meta_level: int = "0", direction: str = "none", palette = "default"):
		'''Generates a custom text sprite. 
		
		Use "/" in the text to force a line break.

		The `color` argument can be a hex color (`"#ffffff"`) or a string (`"red"`).

		The `style` argument may be "noun", "property", or "letter".
		
		The `meta_level` argument should be some number, and applies a metatext filter to the sprite.

		The `direction` argument may be "none", "right", "up", "left", or "down". 
		This can be used with the "property" style to generate directional properties (such as FALL or NUDGE).

		The `palette` argument can be set to the name of a palette.
		'''

		if text in self.banned_words:
			return await self.bot.error(ctx, "Please, don't be rude.")

		# These colors are based on the default palette
		if color:
			real_color = color.lower()
			if real_color.startswith("#"): 
				int_color = int(real_color[1:], base=16) & (2 ** 24 - 1)
				byte_color = (int_color >> 16, (int_color & 255 << 8) >> 8, int_color & 255)
				tile_color = (byte_color[0] / 256, byte_color[1] / 256, byte_color[2] / 256)
			elif real_color.startswith("0x"):
				int_color = int(real_color, base=16)
				byte_color = (int_color >> 16, (int_color & 255 << 8) >> 8, int_color & 255)
				tile_color = (byte_color[0] / 256, byte_color[1] / 256, byte_color[2] / 256)
			elif real_color in valid_colors:
				try:
					palette_img = Image.open(f"data/palettes/{palette}.png").convert("RGB")
					color_index = valid_colors[real_color]
					tile_color = tuple(p/256 for p in palette_img.getpixel(color_index))
				except FileNotFoundError:
					return await self.bot.error(ctx, f"The palette `{palette}` is not valid.")
			else:
				return await self.bot.error(ctx, f"The color `{color}` is invalid.")
		else:
			tile_color = (1, 1, 1)
		if style not in ("noun", "property", "letter"):
			return await self.bot.error(ctx, f"The style `{style}` is not valid. it must be one of `noun`, `property` or `letter`.")
		if direction not in ("none", "up", "right", "left", "down"):
			return await self.bot.error(ctx, f"The direction `{direction}` is not valid. It must be one of `none`, `up`, `right`, `left` or `down`.")
		if "property" == style:
			if direction in ("up", "right", "left", "down"):
				style = style + direction
		if meta_level not in "0123":
			return await self.bot.error(ctx, f"The meta level `{meta_level}` is invalid. It must be one of `0`, `1`, `2` or `3`.")
		try:
			meta_level = int(meta_level)
			buffer = BytesIO()
			tile = Tile(name=text.lower(), color=tile_color, style=style.lower(), custom=True)
			tile.images = self.generate_tile(tile.name, color=tile.color, style=style, meta_level=meta_level)
			self.render([[[tile]]], 1, 1, out=buffer)
			await ctx.send(ctx.author.mention, file=discord.File(buffer, filename=f"custom_'{text.replace('/','')}'.gif"))
		except ValueError as e:
			text = e.args[0]
			culprit = e.args[1]
			reason = e.args[2]
			if reason == "variant":
				return await self.bot.error(ctx, f"The text `{text}` could not be generated, because the variant `{culprit}` is invalid.")
			if reason == "width":
				if len(text) < 20:
					return await self.bot.error(ctx, f"The text `{text}` could not be generated, because it is too long.")
				return await self.bot.error(ctx, f"The text `{text[:20]}` could not be generated, because it is too long.")
			if reason == "char":
				return await self.bot.error(ctx, f"The text `{text}` could not be generated, because no letter sprite exists for `{culprit}`.")
			if reason == "zero":
				return await self.bot.error(ctx, f"The input cannot be empty.")
			if reason == "letter":
				return await self.bot.error(ctx, "You can only apply the letter style for 1 or 2 letter words.")
			return await self.bot.error(ctx, f"The text `{text}` could not be generated.")
		
	@make.command()
	@commands.cooldown(5, 8, type=commands.BucketType.channel)
	async def raw(self, ctx: commands.Context, text: str, style: str = "noun", meta_level: int = 0, direction: str = "none"):
		'''Returns a zip archive of the custom tile.

		A raw sprite has no color!

		The `style` argument may be "noun", "property", or "letter".
		
		The `meta_level` argument should be a number.
		'''
		real_text = text.lower()
		if style not in ("noun", "property", "letter"):
			return await self.bot.error(ctx, f"`{style}` is not a valid style. It must be one of `noun`, `property` or `letter`.")
		if str(meta_level) not in "0123":
			return await self.bot.error(ctx, f"`{meta_level}` is not a valid meta level. It must be one of `0`, `1`, `2` or `3`.")
		if direction not in ("none", "up", "right", "left", "down"):
			return await self.bot.error(ctx, f"`{direction}` is not a valid direction. It must be one of `none`, `up`, `right`, `left` or `down`.")
		if style == "property":
			if direction in ("up", "right", "left", "down"):
				style = style + direction
		try:
			meta_level = int(meta_level)
			buffer = BytesIO()
			images = self.generate_tile(real_text, (1, 1, 1), style, meta_level)
			with zipfile.ZipFile(buffer, mode="w") as archive:
				for i, image in enumerate(images):
					img_buffer = BytesIO()
					image.save(img_buffer, format="PNG")
					img_buffer.seek(0)
					archive.writestr(f"text_{meta_level*'meta_'}{real_text.replace('/','')}_0_{i + 1}.png", data=img_buffer.getbuffer())
			buffer.seek(0)
			await ctx.send(
				f"{ctx.author.mention} *Raw sprites for `text_{meta_level*'meta_'}{real_text.replace('/','')}`*", 
				file = discord.File(buffer, filename=f"custom_{meta_level*'meta_'}{real_text.replace('/','')}_sprites.zip")
			)
		except ValueError as e:
			text = e.args[0]
			culprit = e.args[1]
			reason = e.args[2]
			if reason == "variant":
				return await self.bot.error(ctx, f"The text `{text}` could not be generated, because the variant `{culprit}` is invalid.")
			if reason == "width":
				return await self.bot.error(ctx, f"The text `{text[:20]}` could not be generated, because it is too long.")
			if reason == "char":
				return await self.bot.error(ctx, f"The text `{text}` could not be generated, because no letter sprite exists for `{culprit}`.")
			if reason == "zero":
				return await self.bot.error(ctx, f"The input cannot be empty.")
			if reason == "letter":
				return await self.bot.error(ctx, "You can only apply the letter style for 1 or 2 letter words.")
			return await self.bot.error(ctx, f"The text `{text}` could not be generated.")

	def generate_tile(self, text: str, color: Tuple[int, int], style: str, meta_level: int, seed: Optional[int] = None) -> List[Image.Image]:
		'''
		Custom tile => rendered custom tile
		'''
		if seed is not None:
			random.seed(seed)
		clean = text.replace("/", "")
		forced = len(clean) != len(text)
		size = len(clean)
		final_arrangement = None
		if text.count("/") >= 2:
			raise ValueError(text, None, "slash")
		elif text.count("/") == 1 and size == 1:
			raise ValueError(text, None, "slash")

		if size == 0:
			raise ValueError(text, None, "zero")
		if size == 1:
			if text.isascii() and (text.isalnum() or text == "*"):
				paths = [
					f"data/sprites/{'vanilla' if text.isalnum() else 'misc'}/text_{text}_0".replace("*", "asterisk")
				]
				positions = [
					(12, 12)
				]
			else:
				raise ValueError(text, text, "char")
		elif size == 2 and style == "letter":
			if text.isascii() and all(k.isalnum() or k in "~*" for k in text):
				paths = [
					f"target/letters/thick/text_{text[0]}_0".replace("*", "asterisk"),
					f"target/letters/thick/text_{text[1]}_0".replace("*", "asterisk"),
				]
				positions = [
					(6, 12),
					(18, 12)
				]
			else:
				raise ValueError(text, text, "char")
		elif size > 10:
			raise ValueError(text, None, "width")
		elif style == "letter":
			raise ValueError(text, None, "letter")
		else:
			if size <= 3 and not forced:
				scale = "big"
				data = self.bot.get_cog("Admin").letter_widths["big"]
				# Attempt to have 1 pixel gaps between letters
				arrangement = [24 // size] * size
				positions = [(int(24 // size * (pos + 0.5)), 11) for pos in range(size)]
			else:
				scale = "small"
				data = self.bot.get_cog("Admin").letter_widths["small"]
				if forced:
					split = [text.index("/"), len(clean) - text.index("/")]
					if 0 in split:
						raise ValueError(text, None, "slash")
				else:
					# Prefer more on the top
					split = [(size + 1) // 2, size // 2]
				arrangement = [24 // split[0]] * split[0] + [24 // split[1]] * split[1]
				positions = [(int(24 // split[0] * (pos + 0.5)), 6) for pos in range(split[0])] + \
					[(int(24 // split[1] * (pos + 0.5)), 18) for pos in range(split[1])]
			try:
				widths = {x: data[x] for x in clean}
			except KeyError as e:
				raise ValueError(text, e.args[0], "char")

			
			final_arrangement = []
			for char, limit in zip(clean, arrangement):
				top = 0
				second = 0
				for width in widths[char]:
					if top <= width <= limit:
						second = top
						top = width
				if top == 0:
					raise ValueError(text, char, "width")
				# Add some variety into the result,
				# in case there is only one sprite of the "ideal" width
				if random.randint(0, 1) and second:
					final_arrangement.append(second)
				else:
					final_arrangement.append(top)

			paths = [
				f"target/letters/{scale}/{char}/{width}/" + \
					random.choice(sorted([
						i for i in listdir(f"target/letters/{scale}/{char}/{width}") if i.endswith("0.png")
					]))[:-6]
				for char, width in zip(clean, final_arrangement)
			]
		# Special cases and flags everywhere...
		# No wonder nobody wants to work on this code (including myself)
		# gross
		if size == 2 and style != "letter":
			a, b = final_arrangement # trust me here, pylance
			n_a = 11 - a // 2 if a < 12 else 6
			n_b = 13 + b // 2 if b < 12 else 18
			positions = [(n_a, 11), (n_b, 11)] # center the letters

		
		images = []
		for frame in range(3):
			letter_sprites = [
				Image.open(f"{path}_{frame + (size == 1 or style == 'letter')}.png")
				for path in paths
			]
			letter_sprites = [
				s.getchannel("A") if s.mode == "RGBA" else s.convert("1")
				for s in letter_sprites
			]
			offset_x, offset_y = 0,0
			if style and "property" in style:
				plate = Image.open(f"data/plates/plate_{style}_0_{frame+1}.png").convert("1")
				base = Image.new("1", plate.size, color=0)
				base = ImageChops.invert(ImageChops.add(base, plate))
				offset_x = (plate.width - 24) // 2
				offset_y = (plate.height - 24) // 2
			else:
				base = Image.new("1", (24, 24), color=0)
			for (x, y), sprite in zip(positions, letter_sprites):
				s_x, s_y = sprite.size
				base.paste(sprite, box=(x - s_x // 2 + offset_x, y - s_y // 2 + offset_y), mask=sprite)

			if style and "property" in style:
				base = ImageChops.invert(base)

			base = self.make_meta(clean, base, meta_level).convert("L")

			# Cute alignment
			color_matrix = (color[0], 0, 0, 0,
						 0, color[1], 0, 0,
					  0, 0, color[2], 0,)

			alpha = base.copy()
			base = base.convert("RGB")
			base = base.convert("RGB", matrix=color_matrix)
			base.putalpha(alpha)
			images.append(base)
		
		return images

	async def render_tiles(self, ctx: commands.Context, *, objects: str, rule: bool):
		'''Performs the bulk work for both `tile` and `rule` commands.'''
		async with ctx.typing():
			render_limit = self.bot.render_limit
			tiles = objects.lower().strip().replace("\\", "")
			if tiles == "":
				param = Parameter("objects", Parameter.KEYWORD_ONLY)
				raise commands.MissingRequiredArgument(param)

			# Check for banned words
			for word in self.banned_words:
				if word in tiles:
					return await self.bot.error(ctx, "Please, don't be rude.")

			# Determines if this should be a spoiler
			spoiler = "|" in tiles
			tiles = tiles.replace("|", "")
			
			# Check for empty input
			if not tiles:
				return await self.bot.error(ctx, "Input cannot be blank.")

			# Split input into lines
			word_rows = tiles.splitlines()
			
			# Split each row into words
			word_grid = [row.split() for row in word_rows]

			# Check palette & bg flags
			potential_flags = []
			potential_count = 0
			try:
				for y, row in enumerate(word_grid):
					for x, word in enumerate(row):
						if potential_count == 3:
							raise Exception
						potential_flags.append((word, x, y))
						potential_count += 1
			except: pass
			background = None
			palette = "default"
			raw_flag = False
			to_delete = []
			for flag, x, y in potential_flags:
				bg_match = re.fullmatch(r"(--background|-b)(=(\d)/(\d))?", flag)
				if bg_match:
					if bg_match.group(3) is not None:
						tx, ty = int(bg_match.group(3)), int(bg_match.group(4))
						if not (0 <= tx <= 7 and 0 <= ty <= 5):
							return await self.bot.error(ctx, "The provided background color is invalid.")
						background = tx, ty
					else:
						background = (0, 4)
					to_delete.append((x, y))
					continue
				if re.fullmatch(r"--raw|-r|raw:true", flag):
					raw_flag = True
					to_delete.append((x, y))
					continue
				flag_match = re.fullmatch(r"(--palette=|-p=|palette:)(\w+)", flag)
				if flag_match:
					palette = flag_match.group(2)
					if palette + ".png" not in listdir("data/palettes"):
						return await self.bot.error(ctx, f"Could not find a palette with name \"{palette}\".")
					to_delete.append((x, y))
			for x, y in reversed(to_delete):
				del word_grid[y][x]
			
			try:
				if rule:
					word_grid = split_commas(word_grid, "tile_")
				else:
					word_grid = split_commas(word_grid, "text_")
			except SplittingException as e:
				source_of_exception = e.args[0]
				return await self.bot.error(ctx, f"I couldn't parse the following input: \"{source_of_exception}\".")

			# Splits "&"-joined words into stacks
			for row in word_grid:
				for i,stack in enumerate(row):
					if "&" in stack:
						row[i] = stack.split("&")
					else:
						row[i] = [stack]
					# Limit how many tiles can be rendered in one space
					height = len(row[i])
					if height > 10 and ctx.author.id != self.bot.owner_id:
						return await self.bot.error(ctx, f"Stack too high ({height}).", "You may only stack up to 10 tiles on one space.")

			# Prepends "text_" to words if invoked under the rule command
			if rule:
				word_grid = [[["-" if word == "-" else word[5:] if word.startswith("tile_") else "text_" + word for word in stack] for stack in row] for row in word_grid]
			else:
				word_grid = [[["-" if word in ("-", "text_-") else word for word in stack] for stack in row] for row in word_grid]

			# Get the dimensions of the grid
			lengths = [len(row) for row in word_grid]
			width = max(lengths)
			height = len(word_rows)

			# Don't proceed if the request is too large.
			# (It shouldn't be that long to begin with because of Discord's 2000 character limit)
			area = width * height
			if area > render_limit and ctx.author.id != self.bot.owner_id:
				return await self.bot.error(ctx, f"Too many tiles ({area}).", f"You may only render up to {render_limit} tiles at once, including empty tiles.")
			elif area == 0:
				return await self.bot.error(ctx, f"Can't render nothing.")

			# Pad the word rows from the end to fit the dimensions
			[row.extend([["-"]] * (width - len(row))) for row in word_grid]
			# Finds the associated image sprite for each word in the input
			# Throws an exception which sends an error message if a word is not found.
			
			# Handles variants based on `:` suffixes
			start = time()
			try:
				word_grid = self.handle_variants(word_grid, palette=palette)
			except (ValueError, FileNotFoundError) as e:
				if isinstance(e, FileNotFoundError):
					tile_data = self.bot.get_cog("Admin").tile_data
					tile = e.args[0]
					variants = None
					if ":" in tile:
						variants = ":" + ":".join(tile.split(":")[1:])
						tile = tile.split(":")[0]
					# Error cases
					if tile_data.get(tile) is not None:
						if variants is None:
							return await self.bot.error(ctx, f"The tile `{tile}` exists but a sprite could not be found for it.")
						return await self.bot.error(ctx, f"The tile `{tile}` exists but the sprite variant(s) `{variants}` are not valid for it.")
					if tile_data.get("text_" + tile) is not None:
						if not rule:
							return await self.bot.error(ctx, f"The tile `{tile}` does not exist, but the tile `text_{tile}` does.", "You can also use the `rule` command instead of the `tile command.")
						return await self.bot.error(ctx, f"The tile `{tile}` does not exist, but the tile `text_{tile}` does.")
					if tile.startswith("text_") and tile_data.get(tile[5:]) is not None:
						if rule:
							return await self.bot.error(ctx, f"The tile `{tile}` does not exist, but the tile `{tile[5:]}` does.", "Did you mean to type `tile_{tile}`.")
						return await self.bot.error(ctx, f"The tile `{tile}` does not exist, but the tile `{tile[5:]}` does.")
					return await self.bot.error(ctx, f"The tile `{tile}` does not exist.")
				tile = e.args[0]
				try:
					culprit = e.args[1]
				except IndexError:
					return await self.bot.error(ctx, "Wrong color for true color (hex colouring).")
				reason = e.args[2]
				if reason == "variant":
					return await self.bot.error(ctx, f"The tile `{tile}` could not be automatically generated, because the variant `{culprit}` is invalid.")
				if reason == "width":
					return await self.bot.error(ctx, f"The tile `{tile[:20]}` could not be automatically generated, because it is too long.")
				if reason == "char":
					return await self.bot.error(ctx, f"The tile `{tile}` could not be automatically generated, because no letter sprite exists for `{culprit}`.")
				if reason == "zero":
					return await self.bot.error(ctx, f"Cannot apply variants to an empty tile.")
				if reason == "meta":
					return await self.bot.error(ctx, "You can only go three layers of meta deep.")
				if reason == "letter":
					return await self.bot.error(ctx, "You can only apply the letter style for 1 or 2 letter words.")
				return await self.bot.error(ctx, f"The tile `{tile}` was not found, and could not be automatically generated.")

			# Merges the images found
			buffer = BytesIO()
			timestamp = datetime.now()
			format_string = "render_%Y-%m-%d_%H.%M.%S"
			formatted = timestamp.strftime(format_string)
			filename = f"{formatted}"
			if raw_flag:
				task = partial(self.render, word_grid, width, height, palette=palette, background=background, out="target/render/raw.gif", rand=True, raw=raw_flag)
			else:
				task = partial(self.render, word_grid, width, height, palette=palette, background=background, out=buffer, rand=True)
			try:
				try:
					await self.bot.loop.run_in_executor(None, task)
				except ValueError:
					return await self.bot.error(ctx, f"You can only apply apply three layers of meta.")
			except Exception as e:
				return await self.bot.error(ctx, "There was an exception, during processing your output. Please ensure that your command is correct.\n" + str(e))
			if raw_flag:
				zip_buffer = BytesIO()
				with zipfile.ZipFile(zip_buffer, mode="w") as archive:
					with Image.open("target/render/raw.gif") as im:
						try:
							for i in range(3):
								im.seek(i)
								img_buffer = BytesIO()
								im.save(img_buffer, format="PNG")
								img_buffer.seek(0)
								archive.writestr(filename + f"_0_{i+1}.png", data=img_buffer.getbuffer())
						except:
							return await self.bot.error(ctx, "No images to create.")
				zip_buffer.seek(0)
			delta = time() - start
		# Sends the image through discord
		msg = f"{ctx.author.mention}\n*Rendered in {delta:.2f} s*"
		if raw_flag:
			await ctx.send(content=msg, file=discord.File(zip_buffer, filename=filename + ".gif.zip", spoiler=spoiler))
		else:
			buffer.seek(0)
			await ctx.send(content=msg, file=discord.File(buffer, filename=filename + ".gif", spoiler=spoiler))
		

	@commands.command()
	@commands.cooldown(5, 8, type=commands.BucketType.channel)
	async def rule(self, ctx: commands.Context, *, objects: str = ""):
		'''render the text tiles provided. 
		
		If not found, the bot tries to auto-generate them! (See the `make` command for more.)

		**Flags**
		* `--palette=<...>` (`-P=<...>`): Recolors the output gif. See the `palettes` command for more.
		* `--background` (`-B`): Enables background color.
		
		**Variants**
		* `:variant`: Append `:variant` to a tile to change color or sprite of a tile. See the `variants` command for more.

		**Useful tips:**
		* `-` : Shortcut for an empty tile. 
		* `&` : Stacks tiles on top of each other.
		* `tile_` : `tile_object` render regular objects.
		* `,` : `tile_x,y,...` is expanded into `tile_x tile_y ...`
		* `||` : Marks the output gif as a spoiler. 
		* `#FFFFFF` : Colors the output. See https://htmlcolorcodes.com/color-picker/ for more.
		* Note that hex colors, aka `#FFFFFF` override normal colors and palletes.

		**Example commands:**
		`rule baba is you`
		`rule -B rock is ||push||`
		`rule -P=test tile_baba on baba is word`
		`rule baba eat baba - tile_baba tile_baba:l`
		`rule baba:#9999ff is blurple:property:#9999ff`
		'''
		await self.render_tiles(ctx, objects=objects, rule=True)

	# Generates an animated gif of the tiles provided, using the default palette
	@commands.command()
	@commands.cooldown(5, 8, type=commands.BucketType.channel)
	async def tile(self, ctx: commands.Context, *, objects: str = ""):
		'''render the tiles provided.

	   **Flags**
		* `--palette=<...>` (`-P=<...>`): Recolors the output gif. See the `palettes` command for more.
		* `--background=[...]` (`-B=[...]`): Enables background color.
		
		**Variants**
		* `:variant`: Append `:variant` to a tile to change color or sprite of a tile. See the `variants` command for more.

		**Useful tips:**
		* `-` : Shortcut for an empty tile. 
		* `&` : Stacks tiles on top of each other.
		* `text_` : `text_object` render text objects.
		* `,` : `text_x,y,...` is expanded into `text_x text_y...`
		* `||` : Marks the output gif as a spoiler. 
		* `#FFFFFF` : Colors the output. See https://htmlcolorcodes.com/color-picker/ for more.
		* Note that hex colors, aka `#FFFFFF` override normal colors and palletes.
		
		**Example commands:**
		`tile baba - keke` 
		`tile --palette=marshmallow keke:d baba:s`
		`tile text_baba,is,you`
		`tile baba&flag ||cake||`
		`tile -P=mountain -B baba bird:l`
		`tile baba:#a9999ff is blurple:property:#9999ff`
		'''
		await self.render_tiles(ctx, objects=objects, rule=False)

	@commands.cooldown(5, 8, commands.BucketType.channel)
	@commands.command(name="level")
	async def _level(self, ctx: commands.Context, *, query: str):
		'''render the given Baba Is You level.

		Levels are searched for in the following order:
		* Checks if the input matches a level ID (e.g. "20level")
		* Checks if the input matches a custom level code (e.g. "ABCD-1234")
		* Checks if the input matches a level number (e.g. "space-3" or "lake-extra 1")
		* Checks if the input matches a level name (e.g. "further fields")
		* Checks if the input is a ID of a world (e.g. "cavern")
		'''
		# User feedback
		await ctx.trigger_typing()

		levels = {}
		custom = False
		# Lower case, make the query all nice
		fine_query = query.lower().strip()
		# Is it the level ID?
		level_data = self.bot.get_cog("Reader").level_data
		if level_data.get(fine_query) is not None:
			levels[fine_query] = level_data[fine_query]

		# Check custom level from cache
		if len(levels) == 0:
			upper = fine_query.upper()
			if re.match(r"^[A-Z0-9]{4}\-[A-Z0-9]{4}$", upper):
				custom_levels = self.bot.get_cog("Reader").custom_levels
				if custom_levels.get(upper) is not None:
					levels[upper] = custom_levels[upper]
					custom = True
				else:
					await ctx.send("Searching for custom level... this might take a while")
					await ctx.trigger_typing()
					async with aiohttp.request("GET", f"https://baba-is-bookmark.herokuapp.com/api/level/exists?code={upper}") as resp:
						if resp.status in (200, 304):
							data = await resp.json()
							if data["data"]["exists"]:
								try:
									levels[upper] = await self.bot.get_cog("Reader").render_custom(upper)
									custom = True
								except Exception as e:
									if e.args[0] == "oh frick":
										await self.bot.error(ctx, "The level code is valid, but the level is way too big!")
									else:
										raise

		# Does the query match a level tree?
		if len(levels) == 0:
			# Separates the map and the number / letter / extra number from the query.
			tree = [string.strip() for string in fine_query.split("-")]
			# There should only be two parts to the query.
			if len(tree) == 2:
				# The two parts
				map_id = tree[0]
				identifier = tree[1]
				# What style of level identifier are we given?
				# Style: 0 -> "extra" + number
				# Style: 1 -> number
				# Style: 2 -> letter
				style = None
				# What "number" is the level?
				# .ld files use "number" to refer to both numbers, letters and extra numbers.
				number = None
				if identifier.isnumeric():
					# Numbers
					style = 0
					number = int(identifier)
				elif len(identifier) == 1 and identifier.isalpha():
					# Letters (only 1 letter)
					style = 1
					# 0 <--> a
					# 1 <--> b
					# ...
					# 25 <--> z
					raw_number = try_index(ascii_lowercase, identifier)
					# If the identifier is a lowercase letter, set "number"
					if raw_number != -1: 
						number = raw_number
				elif identifier.startswith("extra") and identifier[5:].strip().isnumeric():
					# Extra numbers:
					# Starting with "extra", ending with numbers
					style = 2
					number = int(identifier[5:].strip()) - 1
				else:
					number = identifier
					style = -1
				if style is not None and number is not None:
					# Custom map ID?
					if style == -1:
						# Check for the map ID & custom identifier combination
						for filename,data in level_data.items():
							if data["map_id"] == number and data["parent"] == map_id:
								levels[filename] = data
					else:
						# Check for the map ID & identifier combination
						for filename,data in level_data.items():
							if data["style"] == style and data["number"] == number and data["parent"] == map_id:
								levels[filename] = data

		# Is the query a real level name?
		if len(levels) == 0:
			for filename,data in level_data.items():
				# Matches an existing level name
				if data["name"] == fine_query:
					# Guaranteed
					levels[filename] = data

		# Map ID?
		if len(levels) == 0:
			for filename,data in level_data.items():
				try:
					if data["map_id"] == fine_query and data["parent"] is None:
						levels[filename] = data
				except:
					return self.bot.error(ctx, "No level named " + map_id + ".")

		# If not found: error message
		if len(levels) == 0:
			return await self.bot.error(ctx, f'Could not find a level matching the query "{fine_query}".')

		# If found:
		else:
			# Is there more than 1 match?
			matches = len(levels)

			level_id, level = [*levels.items()][0]
			# 'source', 'name', 'subtitle'? checked for both vanilla & custom
			# 'parent', 'map_id', 'style', 'number' checked for vanilla

			# The embedded file
			gif = discord.File(f"target/renders/{level['source']}/{level_id}.gif", spoiler=True)
			
			# Level name
			name = level["name"]

			# Level parent 
			if not custom:
				parent = level.get("parent")
				map_id = level.get("map_id")
				tree = ""
				# Parse the display name
				if parent is not None:
					# With a custom map id
					if map_id is not None:
						# Format
						tree = parent + "-" + map_id + ": "
					else:
						# Parse the level style
						style = level["style"]
						number = level["number"]
						identifier = None
						# Regular numbers
						if style == 0:
							identifier = number
						elif style == 1:
						# Letters
							identifier = ascii_lowercase[int(number)]
						elif style == 2:
						# Extra dots
							identifier = "extra " + str(int(number) + 1)
						else: 
						# In case the custom map ID wasn't already set
							identifier = map_id
						# format
						tree = f"{parent}-{identifier}: "
			
			if custom:
				author = f"\nAuthor: `{level['author']}`"

			# Level subtitle, if any
			subtitle = ""
			if level.get("subtitle") and level.get("subtitle").strip():
				subtitle = "\nSubtitle: `" + level["subtitle"] + "`"

			# Any additional matches
			matches_text = "" if matches == 1 else f"\nFound {matches} matches: `{', '.join([l for l in levels])}`, showing the first." 

			# Formatted output
			if custom:
				formatted = f"{ctx.author.mention}{matches_text} (Custom Level)\nName: `{name}`\nCode: `{level_id}`{author}{subtitle}"
			else:
				formatted = f"{ctx.author.mention}{matches_text}\nName: `{tree}{name}`\nID: `{level_id}`{subtitle}"

			# Only the author should be mentioned
			mentions = discord.AllowedMentions(everyone=False, users=[ctx.author], roles=False)

			# Send the result
			await ctx.send(formatted, file=gif, allowed_mentions=mentions)

def setup(bot: commands.Bot):
	bot.add_cog(GlobalCog(bot))
