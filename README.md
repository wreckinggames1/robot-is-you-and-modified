# About

A fun Discord bot based on the indie game [Baba Is You](https://store.steampowered.com/app/736260/Baba_Is_You/) (by Arvi Teikari). This bot was written with the [discord.py](https://discordpy.readthedocs.io/en/latest/) library for Python 3.x.

# Functionality

A list of all commands can be seen using the `help` command. (By default, the bot has a prefix of `+`.)

This bot primarily features rendering and animation of
tiles in *Baba Is You* (through the `tile` and `rule` commands), 
as well as *Baba Is You* levels (using the `level` command).

*Example command:*

![Example command](https://media.discordapp.net/attachments/764512683461967937/810905871705047040/unknown.png)

**NOTE: Output is in the form of an animated gif. Try the commands yourself for the best experience.**

# Support server.

Well, there is no server, ( as far as I don't make one ), so any bugs can be sent to me in discord directly: Niki4tap#6227.

# To Host This Yourself

Ah, here, so, you can host modified bot [(mine)](https://gitlab.com/niki4tap/robot-is-you-and-modified), or unmodified bot [(RocketRace)](https://github.com/RocketRace/robot-is-you)

So, modified bot is unstable, and can lose support of the original creator. But, modified bot have a little bit more functions. So you have to choose.

Please follow the terms of the license!

Install the requirements: `pip install -r requirements.txt`.

Run the bot using `python3 ROBOT.py`.

The bot should work fine on windows too, if you will notice any issues running the bot on specific platform, report this to me on discord.

## Setup

### Make

Make is a simple setup script for bot, there are two versions of it:

For windows: make.ps1 (powershell script)

For posix: make.sh (bash/sh script)

`make` shell script, located in the root of repository, should be used to generate basic files folder for the bot.

Use: `make <bot's token>`

Although, it won't setup everything, host still needs to generate and fill webhook id and owner id.


### Setting up a webhook

Any unhandled error will be posted through webhook, so it's better to set it up in some mod channel.

Go to `Edit channel` > `Integrations` > `Webhooks` > `New Webhook` > `Copy Webhook Url`

![Example](https://cdn.discordapp.com/attachments/764512683461967937/817146636144345109/unknown.png)

You'll end up with this url in your clipboard: `https://discord.com/api/webhooks/some-numbers/some-gibberish-data`

Copy ONLY `some-numbers` section of this url, and paste it in `webhook_id`, of config/setup.json file

### Required files

Those should be generated automaticly with `make.sh`, but if you can't generate them automaticly, you will need to create them manually.

Bot configuration is in `config/setup.json`. It contains the following fields:
* `render_limit` An integer, that defines maximum amount of tiles per render.
* `auth_file` Should point to a JSON file with a `token` field with your bot token.
* `activity` A "playing" message to set at login.
* `description` A description to use in the help command.
* `prefixes` A list of strings that can be used to trigger commands.
* `trigger_on_mention` A boolean dictating whether bot @mentions will behave as a command prefix.
* `webhook_id` The ID of a webhook to report command errors to. Requires the `manage webhooks` permission in the webhook's channel.
* `owner_id` The ID of the bot owner.
* `embed_color` The color of embedded messages.
* `log_file` The file to report logs to.
* `cogs` A list of strings -- cogs to load into the bot.

If the bot complains about missing files or directories in `cache/` or `target/`, create them. Specifically, you should have the following paths created:

* `cache/tiledata.json` A JSON file defaulting to the empty object `{}`. Contains tile data for rendering objects.
* `cache/blacklist.json` A JSON file defaulting to the empty object `{}`. Contains the IDs of blocked users.
* `cache/leveldata.json` A JSON file defaulting to the empty object `{}`. Contains level metadata.
* `cache/debug.json` A JSON file defaulting to the simple object: `{"identifies": [], "resumes": []}`. Contains debug data about bot restarts.
* `target/renders/vanilla/` An empy directory. Contains levels rendered into animated GIFs.
* `target/letters/big/` An empty directory. Contains sprites for "tall" letters (like `IS`) scraped from vanilla sprites.
* `target/letters/small/` An empty directory. Contains sprites for "short" letters (like `BABA`) scraped from vanilla sprites.
* `target/letters/thick/` An empty directory. Contains sprites for "individual" letters (like `BA`) scraped from vanilla sprites.

## Setup commands (bot owner only)

`<>` denotes a required argument, and `[]` denotes an optional argument.

* `loaddata` Collects tile metadata from `values.lua`, `editor_objectlist.lua`, `data/worlds/vanilla/*.ld` files and `data/custom/*.json` files, and saves it to disk. The following commands are also available, but it is **strongly recommended** to use `loaddata`.
* * `loadchanges` Collects tile metadata only from `.ld` files.
* * `loadcolors` Collects tile metadata only from `values.lua`.
* * `loadcustom` Collects tile metadata only from custom `.json` files.
* * `loadeditor` Collects tile metadata only from `editor_objectlist.lua`.
* * `dumpdata` Dumps collected tile metadata into `cache/tiledata.json`.
* `loadletters` Scrapes individual letter sprites from image sprites in `data/sprites/*`, as well as pre-made letters from `data/letters/**/*` and places the results in `target/letters/`.
* `loadmap <world_name> <level_id> [include_metadata?]` Reads and renders an animated GIF of the provided level. `world_name` should be `vanilla` in most cases. If `include_metadata` is `True`, the level metadata is stored as well. Useful for re-rendering levels changed in an update without re-doing everything.
* `loadmaps` Reads and renders every single level in `data/levels/vanilla/`. Also collects metadata.
* `render_limit <number>` Sets and saves new render_limit in `cache/render.json`.

* To load tile data, run the `loaddata` command. To load letter data (for custom text), run the `loadletters` command. To load and pre-render levels, run the `loadmaps` command.

## Adminstrative commands (bot owner only)

`<>` denotes a required argument, and `[]` denotes an optional argument.

* `load [cog]`(aliases: `reload`, `reloadcog`) Reloads a cog. Useful to hot-reload modules of the bot. If the argument is omitted, all cogs are reloaded.
* `restart` Exits the bot with a return code of 1. (I use this with a process manager that restarts failed tasks.)
* `logout` (aliases: `kill`, `yeet`) Exits the bot with a return code of 0. 
* `debug` Gives some debug information about the bot health, including the number of IDENTIFY and RESUME payloads in the past 24 hours.
* `ban <user_id>` Adds a user ID to the list of blacklisted users.
* `unban <user_id>` Reverts the ban command.
* `leave <guild_id>` Leaves a guild.
* `hidden` Lists all hidden commands.
* `doc <command>` Displays the docstring for a command.
* `servers` Lists all servers, where bot is present.

The bot additionally uses [Jishaku](https://github.com/Gorialis/jishaku/) to interface with `git`, run shell commands and evaluate python. Read more about the `jsk` command at [Jishaku's documentation](https://jishaku.readthedocs.io/en/latest/).
